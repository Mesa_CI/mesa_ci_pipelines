How to use these scripts:

0) If a remote branch does not yet exist for the branch you wish to push to, you'll need to follow these preliminary steps:
	0a) checkout a new local branch with the name of the branch you'd like to push to
	0b) git reset the branch to a suitable initial commit
	0c) push to the remote branch of this local branch: ```git push upstream <name_of_branch_>```
	0d) proceed with the remaining steps as normal

1) run ```python fetch_branchdirs.py``` to setup worktrees. You will use these to transmit changes from localdirs to upstream.
   localdirs: staging directories that contain changes you'd like to make to their corresponding upstream branches.
   (eg. localdir dev_ngcortes maps to upstream branch dev_ngcortes)
   wortrees: generally you shouldn't modify these directly. These are where changes are copied from localdirs and pushed to the upstream remote

2) create a jinja2 compatible template in the templates directory that will be a valid Jenkinsfile when the template is applied

3) Add an entry to jenkinsfile_branch_map.yaml mapping your template to a corresponding branch on upstream (eg. dev_tpalli)

4) run ```python apply_templates.py --stage```
   to generate a jenkinsfile using your template, copy it to its subdir in the localdirs directory,
   and stage the generated files for committing to this branch (via ```git add```).
   NB: You can also exclude the ```--stage``` option if you'd like to stage the files manually.

5) commit your work with ```git commit```, then verify all files look correct with ```git show```

6) run ```python push_localdirs.py``` to see what changes will be made when you push everything to upstream.

7) if everything looked good in 6), run ```python push_branchdirs.py --no-dryrun```

===============================================================================

FAQ:

"How do I add a local copy of a worktree to the main branch?"

1) create a directory in localdirs for the worktree manually, then copy the Jenkinsfile into the localdir from the worktree

2) stage and commit the file to the main branch, then push to upstream/main

===============================================================================

"How do I edit a worktree's commit index and force push?"

1) nagivate to the worktree and do any doctoring the to commit index you need to

2) push to the branch directly from the worktree
