node('master') {
  properties([
    disableConcurrentBuilds(),
    buildDiscarder(logRotator(daysToKeepStr: '50', numToKeepStr: '300')),
    parameters([
      string(name: 'name', defaultValue: 'manually_started', description: 'this parameter is set to the relevant commit when builds are triggered'),
      string(name: 'type', defaultValue: 'percheckin', description: 'developer/daily/percheckin/release type specified to the test harnesses'),
      string(name: 'revision', defaultValue: '', description: 'eg. mesa=<sha1> piglit=<sha1> ...'),
      string(name: 'build_support_branch', defaultValue: 'origin/master', description: 'specifies the branch of mesa_jenkins scripts used for the build'),
      string(name: 'notify', defaultValue: 'konstantin.seurer@gmail.com'),
      booleanParam(name: 'rebuild', defaultValue: false, description: 'Rebuild all components?'),
    ])])
  buildName "${BUILD_NUMBER}-${params.name}-${params.type}"
  checkout([$class: 'GitSCM', branches: [[name: "${params.build_support_branch}" ]],
	          userRemoteConfigs: [[name: 'origin', url: 'git://otc-mesa-ci.local/git/mirror/mesa_jenkins/origin'],
				                        [name: 'majanes', url: 'git://otc-mesa-ci.local/git/mirror/mesa_jenkins/majanes'],
				                        [name: 'ybogdano', url: 'git://otc-mesa-ci.local/git/mirror/mesa_jenkins/ybogdano'],
				                        [name: 'ngcortes', url: 'git://otc-mesa-ci.local/git/mirror/mesa_jenkins/ngcortes']]])
  stage('clear stale testing info') {
    sh '''
      rm -rf results/test
      rm -rf summary.xml
      rm -rf test_summary.txt
    '''
  }
  withCredentials([usernamePassword(credentialsId: 'jenkins_auth', usernameVariable: 'JENKINS_USER', passwordVariable: 'JENKINS_PW')]) {
    try {
      timestamps {
	      timeout(activity: true, time: 2, unit: 'HOURS') {
	        stage('fetch sources') {
	          sh "python3 -u fetch_sources.py --branch=${JOB_BASE_NAME} ${params.revision}"
	        }
	        stage('execute test suites') {
	          sh "python3 -u repos/mesa_ci/scripts/build_jenkins.py --branch=${JOB_BASE_NAME} --type=${params.type} --rebuild=${params.rebuild}"
	        }
	      }
      }
    } catch (err) {
      if (err instanceof InterruptedException) {
        echo "Build was interrupted. Cancelling child jobs..."
        stage('cancel child test runs') {
			    sh "python3 -u repos/mesa_ci/scripts/abort_builds.py"
		    }
      }

      echo "ERROR: Build failed: ${err}"
      currentBuild.result = 'FAILURE'
    }
    try {
      stage('publish results') {
	      archiveArtifacts allowEmptyArchive: true, artifacts: "summary.xml,results/test/stripped/piglit*xml,results/test/card_error*,results/test/core.*"
	      junit allowEmptyResults: true, testResults: "results/test/stripped/piglit*xml"
	      step([$class: 'ACIPluginPublisher', name: 'summary.xml,results/test/card_error*', shownOnProjectPage: false])
      }
    } catch (err) {
      echo "ERROR: could not publish results: ${err}"
    }

    if (params.notify) {
      stage('send email alert') {
	      mail to: params.notify,
	        subject: "Mesa CI completed with status ${currentBuild.currentResult}: $JOB_BASE_NAME ${currentBuild.displayName}",
	        body: """See ${BUILD_URL}
            Results at https://mesa-ci.01.org/${JOB_BASE_NAME}/builds/${BUILD_NUMBER}/group/63a9f0ea7bb98050796b649e85481845
          """
      }
    }
  }
}
