node('master') {
  properties([
    throttleJobProperty(maxConcurrentTotal: 4, throttleEnabled: true, throttleOption: 'project'),
    buildDiscarder(logRotator(daysToKeepStr: '30')),
    parameters([
      string(name: 'result_path'),
      string(name: 'mesa_ci_branch', defaultValue: 'origin/master'),
      booleanParam(name: 'all_builds', defaultValue: false, description: 'Import all builds from job path. eg. from /mnt/space/results/dev_ngcortes/')
    ])
  ])
  buildName "${BUILD_NUMBER}"
  checkout([$class: 'GitSCM', branches: [[name: "${params.mesa_ci_branch}" ]],
	    userRemoteConfigs: [[url: 'https://gitlab.freedesktop.org/Mesa_CI/mesa_ci.git'],
				[url: 'git://otc-mesa-ci.local/git/mirror/mesa_ci/majanes']]])
  stage('clear stale testing info') {
    sh '''
                    rm -rf results/test
                    rm -rf summary.xml
                    rm -rf test_summary.txt
                '''
  }
  withCredentials([usernamePassword(credentialsId: 'jenkins_auth', usernameVariable: 'JENKINS_USER', passwordVariable: 'JENKINS_PW')]) {
    try {
      timestamps {
	timeout(activity: true, time: 30, unit: 'MINUTES') {
	  stage('execute test suites') {
	    sh "MESA_CI_RESULT_HOST_PATH=/tmp/mesa_ci_results MESA_CI_RESULT_HOST=mesa-ci-results.local MESA_CI_RESULT_HOST_PORT=2222 python3 -u scripts/export_results.py --internal ${params.all_builds ? '--all_builds' : ''} ${params.result_path}"
	  }
	}
      }
    }
    catch (error) {
      echo "ERROR: Build failed: ${error}"
      currentBuild.result = 'FAILURE'
    }
    stage('send email alert') {
      mail to: 'mesa_ci@eclists.intel.com',
	subject: "private results posted: ${JOB_BASE_NAME} ${currentBuild.displayName}",
	body: "See ${BUILD_URL}"
    }
  }
}
