node('master') {
  properties([
    buildDiscarder(logRotator(daysToKeepStr: '300')),
    throttleJobProperty(categories: [], limitOneJobWithMatchingParams: false, maxConcurrentPerNode: 1,
			maxConcurrentTotal: 0, paramsToUseForLimit: '', throttleEnabled: true, throttleOption: 'project'),
    parameters([
      string(name: 'build_support_branch', defaultValue: 'origin/master', description: 'specifies the branch of mesa_jenkins scripts used for the build'),
      string(name: 'label', defaultValue: 'None', description: 'specifies machine(s) to install kernel to (eg. bdw or bdwgt2-03)'),
      booleanParam(name: 'rebuild', defaultValue: false, description: 'Rebuild all components?'),
      booleanParam(name: 'build_xe', defaultValue: true, description: 'Build xe kernel or i915? default: xe'),
      string(name: 'revision', defaultValue: '', description: 'eg. linux=123456789')
    ])
  ])
  checkout([$class: 'GitSCM', branches: [[name: "${params.build_support_branch}" ]],
	    userRemoteConfigs: [[name: 'origin', url: 'git://otc-mesa-ci.local/git/mirror/mesa_jenkins/origin'],
				[name: 'majanes', url: 'git://otc-mesa-ci.local/git/mirror/mesa_jenkins/majanes'],
				[name: 'ybogdano', url: 'git://otc-mesa-ci.local/git/mirror/mesa_jenkins/ybogdano'],
				[name: 'ngcortes', url: 'git://otc-mesa-ci.local/git/mirror/mesa_jenkins/ngcortes']]])
  buildName "deploy-kernel-${env.BUILD_NUMBER}-${params.label}"
  withCredentials([usernamePassword(credentialsId: 'jenkins_auth', usernameVariable: 'JENKINS_USER', passwordVariable: 'JENKINS_PW')]) {
    try {
      def build_xe = params.build_xe ? "1" : "0"
      timestamps {
	timeout(time: 145, unit: 'MINUTES') {
	  stage('fetch sources') {
            sh "python3 -u fetch_sources.py --project=linux ${params.revision}"
          }
	  stage('build and deploy linux kernel') {
            sh """python3 -u repos/mesa_ci/scripts/build_jenkins.py \
                    --project=linux --hardware=builder \
                    --type=release --rebuild=${params.rebuild} \
                    --env=\\\"BUILD_XE=${build_xe}\\ INSTALL_ON=${params.label}\\ BUILD_NUMBER=${env.BUILD_NUMBER}\\\"
            """
          }
	}
      }
    }
    catch (error) {
      echo "ERROR: Build failed: ${error}"
      currentBuild.result = 'FAILURE'
    }
  }
}
