#!/usr/bin/env python

from jinja2 import Environment, FileSystemLoader
import os
import yaml
import git
import sys
import argparse

from fetch_branchdirs import fetch_branchdirs

argparser = argparse.ArgumentParser(description=
                                    "apply jinja templated jenkinsfiles to jenkins jobs "
                                    "listed in jenkinsfile_branch_map.yaml",
                                    formatter_class=argparse.RawTextHelpFormatter)
argparser.add_argument("--stage", action="store_true", help="add any changed files to the local repo's index to prepare for committing")

args = argparser.parse_args()
repo_url = "git@gitlab.freedesktop.org:Mesa_CI/mesa_ci_pipelines.git"
local_repo = git.Repo()
environment = Environment(loader=FileSystemLoader("templates/"))

try:
    os.makedirs("localdirs")
except FileExistsError:
    pass

with open("jenkinsfile_branch_map.yaml", "r") as stream:
    jf_br_map = yaml.safe_load(stream)

    for jf_fname, jf_branches in jf_br_map.items():
        template = environment.get_template(jf_fname)

        for br_name, br_map in jf_branches.items():
            local_dir = f"localdirs/{br_name}"
            local_jf_path = f"{local_dir}/Jenkinsfile"

            try:
                os.makedirs(local_dir)
            except FileExistsError:
                pass

            print(f"writing Jenkinsfile to {local_dir}")
            with open(local_jf_path, "w") as jf:
                print(template.render(br_map), file=jf)

            if not local_repo.is_dirty(untracked_files=True, path=local_jf_path):
                print(f"WARN: file didn't change when writing to {local_jf_path}.")
                continue
            elif args.stage:
                print(f"staging {local_dir} to local repo")
                local_repo.index.add(local_dir)
