#!/usr/bin/env python

import os
import git
import shutil
import argparse

repo_url = "git@gitlab.freedesktop.org:Mesa_CI/mesa_ci_pipelines.git"

def fetch_branchdirs(reset=False, brs2fetch=[]):
    local_repo = git.Repo()

    try:
        upstream_remote = local_repo.remote("upstream")
    except ValueError:
        print("upstream remote does not exist, creating...")

        local_repo.create_remote("upstream", repo_url)
        upstream_remote = local_repo.remote("upstream")

    try:
        os.makedirs("worktrees")
    except FileExistsError:
        pass

    upstream_remote.fetch()
    for upstream_br in upstream_remote.refs:
        br_basename = os.path.basename(upstream_br.name)

        wt_dir = f"worktrees/{br_basename}"

        if br_basename == "main" or br_basename == "HEAD":
            continue

        if brs2fetch and (br_basename not in brs2fetch):
            continue

        if reset:
            print(f"WARN: overwriting contents of {wt_dir} with NO checks")
            try:
                shutil.rmtree(wt_dir)
            except FileNotFoundError:
                pass
            except Exception as error:
                print(error)
                print(f"couldn't overwrite {wt_dir}. Skipping...")
                continue

        try:
            wt_repo = git.Repo(wt_dir)
        except git.exc.NoSuchPathError:
            if os.path.abspath(wt_dir) in worktree_list(os.getcwd(), path_only=True):
                print(f"{wt_dir} was found in the local repo's worktree list "
                      "but its directory wasn't found. removing worktree record...")
                shutil.rmtree(f"{local_repo.git_dir}/{wt_dir}")
            print(f"creating worktree at {wt_dir}")
            local_repo.git.worktree("add", wt_dir, upstream_br.name)
            wt_repo = git.Repo(wt_dir)
        except git.exc.InvalidGitRepositoryError:
            print(f"{wt_dir} is not a valid worktree. Skipping...")
            continue
        except Exception as error:
            print(error)
            print(f"could not create repo object for {wt_dir}. Skipping...")
            continue

        try:
            wt_br_head = wt_repo.heads[br_basename]
        except IndexError:
            print(f"branch {br_basename} does not exist on its worktree, creating...")
            wt_repo.create_head(br_basename)
            wt_br_head = wt_repo.heads[br_basename]
        except Exception as error:
            print(error)
            print(f"could not find {br_basename} in {wt_dir} repo. Skipping...")
            continue

        try:
            wt_br_head.checkout()
            print(f"checked out {br_basename} local branch in {wt_dir}")
        except git.exc.RepositoryDirtyError:
            print(f"{wt_dir} has uncommitted changes. "
                  "Skipping setup and checkout of local branch...")
            continue
        except Exception as error:
            print(error)
            print(f"could not checkout {br_basename} in {wt_dir}. Skipping...")
            continue

        try:
            wt_repo.git.rebase(f"upstream/{br_basename}")
            print(f"rebased upstream updates onto {wt_dir}")
        except Exception as error:
            print(error)
            print(f"could not rebase {wt_dir} on upstream branch. Skipping...")
            continue

def worktree_list(repo_dir, path_only=True):
    try:
        repo = git.Repo(repo_dir)
    except Exception as error:
        print(error)
        print(f"failed to create repo object for {repo_dir}. skipping...")
        return

    worktrees = repo.git.worktree("list").split("\n")

    if path_only:
        return list(map(lambda l: l.split()[0], worktrees))
    else:
        return worktrees

if __name__ == '__main__':
    argparser = argparse.ArgumentParser(description="create and/or fetch all worktrees of jenkins jobs in CI")
    argparser.add_argument("--reset", action="store_true", help=
                           "OVERWRITE AND RESET worktrees' contents "
                           "and git history if needed. ")
    argparser.add_argument("--branches",
                          nargs="+",
                          default=[],
                          help="list of upstream branches to fetch"
                           "eg. --branches dev_conformance dev_foobar...")

    args = argparser.parse_args()
    fetch_branchdirs(reset=args.reset, brs2fetch=args.branches)
